# OpenML dataset: Diversity_in_Tech_Companies

https://www.openml.org/d/46090

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The "Diversity in Tech Companies" dataset offers an insightful exploration into the ethnic and gender composition of the workforce within major technology firms over recent years. This dataset comprises data spanning from 2014 to 2018, presenting a compelling overview of diversity trends within the tech industry. It serves as a valuable resource for researchers, policymakers, and industry professionals striving to understand and enhance diversity and inclusion within the technology sector.

Attribute Description:
- **Year**: The calendar year the data was collected, ranging from 2014 to 2018.
- **Company**: Names of the tech companies included in the dataset (e.g., Cisco, Yahoo!, Nvidia, Microsoft, Netflix).
- **Female %**: Percentage of the workforce identified as female.
- **Male %**: Percentage of the workforce identified as male.
- **% White**: Percentage of the workforce identified as White.
- **% Asian**: Percentage of the workforce identified as Asian.
- **% Latino**: Percentage of the workforce identified as Latino.
- **% Black**: Percentage of the workforce identified as Black.
- **% Multi**: Percentage of the workforce identified as belonging to two or more races.
- **% Other**: Percentage of the workforce identified under categories not explicitly listed.
- **% Undeclared**: Percentage of the workforce that has not declared their ethnic background.

Use Case:
This dataset is primed for analysis aimed at uncovering insights into how diversity within the tech industry has evolved over the specified years. It can support a broad range of applications, from academic research focusing on workplace diversity to strategizing corporate policies aimed at fostering a more inclusive environment. Additionally, it could aid in societal discourse on the importance of diversity in innovation-driven sectors, helping stakeholders to benchmark progress and identify areas requiring concerted efforts and improvements.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46090) of an [OpenML dataset](https://www.openml.org/d/46090). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46090/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46090/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46090/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

